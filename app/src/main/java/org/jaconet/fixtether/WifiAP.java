package org.jaconet.fixtether;

/**
Part of code is based on that provided by http://stackoverflow.com/a/7049074/1233435
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;
/**
 * Handle enabling and disabling of WiFi AP
 * @author 
 */
@SuppressLint("WorldWriteableFiles")
public class WifiAP extends MainActivity {
	public ArrayList<String> iprules; 
    private static int WIFI_AP_STATE_UNKNOWN = -1;
    private static int WIFI_AP_STATE_DISABLING = 0;
    private static int WIFI_AP_STATE_DISABLED = 1;
    private static int WIFI_AP_STATE_ENABLING = 2;
    private static int WIFI_AP_STATE_ENABLED = 3;
    private static int WIFI_AP_STATE_FAILED = 4;

    private final String[] WIFI_STATE_TEXTSTATE = new String[] {"DISABLING","DISABLED","ENABLING","ENABLED","FAILED" };
 
    private WifiManager wifi;
    private String TAG = "WifiAP";
     
     public void toggleWiFiAP(WifiManager wifihandler, Context context) 
    {
     	if (wifi==null)    	{    wifi = wifihandler;     }
        boolean wifiApIsOn = getWifiAPState()==WIFI_AP_STATE_ENABLED || getWifiAPState()==WIFI_AP_STATE_ENABLING;
        new SetWifiAPTask(!wifiApIsOn,context).execute();
    }

    private int setWifiApEnabled(boolean enabled) {
    	Log.d(TAG, "Request to enable wifi " + enabled);
    	//disable wireless.
    	if (enabled && wifi.getConnectionInfo() !=null ) 
    	{
    		Log.d(TAG, "disabling wifi");
    		wifi.setWifiEnabled(false);
    		int loopMax = 10;
    		while(loopMax>0 && wifi.getWifiState()!=WifiManager.WIFI_STATE_DISABLED)
    		{
    			Log.d(TAG, "Wifi disabling step: " + (10-loopMax));
    			try { Thread.sleep(500);  loopMax--; } catch (Exception e) { Log.e(TAG,"sleep exception"); }
    		}
    	}

    	//Set wifi
    	setWifiAPState(enabled );  
    	//wait to be completed
    	int loopMax = 10;
    	if (!enabled) 
    	{
    		while (loopMax>0 && (getWifiAPState()==WIFI_AP_STATE_DISABLING || getWifiAPState()==WIFI_AP_STATE_ENABLED || getWifiAPState()==WIFI_AP_STATE_FAILED)) 
    		{
    			Log.d(TAG, (enabled?"enabling":"disabling") +" wifi ap waiting step: " + (10-loopMax));
    			try { Thread.sleep(500);  loopMax--; } catch (Exception e) { Log.e(TAG,"sleep exception"); }
    		}
    		//enable wifi this is somewhat unreliable and app gets confused and doesn't turn it back on sometimes so added toggle to always enable if you desire
    		Log.d(TAG, "enable wifi: calling");
    		wifi.setWifiEnabled(true);
    		loopMax=10;
    		//Wait WiFi to be enabled again. This is needed since application may finish too soon.
    		while(loopMax>0 && wifi.getWifiState()!=WifiManager.WIFI_STATE_ENABLED)
    		{
    			Log.d(TAG, " wifi enabling step: " + (10-loopMax));
    			try { Thread.sleep(500);  loopMax--; } catch (Exception e) { Log.e(TAG,"sleep exception"); }
    		}
    	} 
    	else  
    	{
    		while (loopMax>0 && (getWifiAPState()==WIFI_AP_STATE_ENABLING || getWifiAPState()==WIFI_AP_STATE_DISABLED || getWifiAPState()==WIFI_AP_STATE_FAILED)) 
    		{
    			if (getWifiAPState()==WIFI_AP_STATE_FAILED)    setWifiAPState(enabled );  //jaco: if state is failed then command need to be sent again
    			Log.d(TAG, (enabled?"enabling":"disabling") +" wifi ap: waiting, pass: " + (10-loopMax));
    			try { Thread.sleep(500);  loopMax--; } catch (Exception e) { Log.e(TAG,"sleep exception"); }
    		}
    		if (getWifiAPState()==WIFI_AP_STATE_ENABLED) WriteIpTables();  //jaco: set iptables       		
    	}
    	return getWifiAPState();
    }

    private void setWifiAPState(boolean enabled)
    {
	       try 
	       {
	            Method method = wifi.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
	            method.invoke(wifi, null, enabled); // true
	        } catch (Exception e) {     Log.e(WIFI_SERVICE, "Exception while getting methon setWifiApEnabled");   }  //Previous implementation was causing an Exception
    	
    }
        public int getWifiAPState() 
    {
        int state = WIFI_AP_STATE_UNKNOWN;
        int constant = 0;
        try 
        {
            Method method = wifi.getClass().getMethod("getWifiApState");
            state = (Integer) method.invoke(wifi);
        } catch (Exception e) {   Log.e(WIFI_SERVICE, e.getMessage());       }
        if(state>=10){ constant=10; } //using Android 4.0+ (or maybe 3+, haven't had a 3 device to test it on) so use states that are +10
        //reset these in case was newer device
        WIFI_AP_STATE_DISABLING = 0+constant;
        WIFI_AP_STATE_DISABLED = 1+constant;
        WIFI_AP_STATE_ENABLING = 2+constant;
        WIFI_AP_STATE_ENABLED = 3+constant;
        WIFI_AP_STATE_FAILED = 4+constant;
        Log.d(TAG, "getWifiAPState.state " + (state==-1?"UNKNOWN":WIFI_STATE_TEXTSTATE[state-constant]));
        return state;
    }

    /**
     * the AsyncTask to enable/disable the wifi ap
     * @author http://stackoverflow.com/a/7049074/1233435
     */
    class SetWifiAPTask extends AsyncTask<Void, Void, Void> 
    {
        private boolean mMode; //enable or disable wifi AP
        ProgressDialog d;

        public SetWifiAPTask(boolean mode,  Context context) 
        {
        	mMode = mode;
        	d = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() 
        {
            super.onPreExecute();
            d.setTitle("Turning WiFi AP " + (mMode?"on":"off") + "...");
            d.setMessage("...please wait a moment.");
            d.show();
        }

        /**
         * do after background task runs
         * @param aVoid
         * @author http://stackoverflow.com/a/7049074/1233435
         */
        @Override
        protected void onPostExecute(Void aVoid) 
        {
            super.onPostExecute(aVoid);
            try { d.dismiss(); } catch (IllegalArgumentException e) { };
            Log.i(TAG,"All done ready to kill my pid");
            android.os.Process.killProcess(android.os.Process.myPid()); //TODO: this way is rude, anything better?
           	finish();
        }

        @Override
        protected Void doInBackground(Void... params) 
        {
        	setWifiApEnabled(mMode);
        	return null;
        }
    }
    
    private void WriteIpTables()
    {

    	
    	String line;
	    try{
	    	ArrayList<String> commandLine = new ArrayList<String>();
	    	for (String rule:iprules){
		    	commandLine.clear();
		    	commandLine.add("su");
		        commandLine.add("-c");
		        commandLine.add("iptables "+rule);
	
		        Process process = Runtime.getRuntime().exec(commandLine.toArray(new String[0]));
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
	            Log.i("WifiAP", commandLine.toString());
		        while ((line = bufferedReader.readLine()) != null){ Log.i("WifiAP", line+"\n");}
		        }
		        
	    	
	    }  catch (IOException e){ Log.e("WifiAP", "Fail: " + e.getMessage());   } 
        try { Thread.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); }
    }
    
}