package org.jaconet.fixtether;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    static WifiAP wifiAp;
    
    private WifiManager wifi;
	
     @Override
    protected void onCreate(Bundle savedInstanceState) 
     {
 		Log.d("WifiAP","iptable file stored here: "+this.getApplicationContext().getFilesDir().toString());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        wifiAp = new WifiAP();
        wifiAp.iprules = new ArrayList<String>();
    	try {
			BufferedReader  buf =new BufferedReader(new FileReader(this.getApplicationContext().getFilesDir().toString()+"/wifi_rules"));
			Log.i("WifiAP","Reading default cfg");
			String rule;
			while ((rule=buf.readLine()) != null) if(!rule.startsWith("#"))	wifiAp.iprules.add(rule);
			buf.close();
		} catch (FileNotFoundException e1) 
			{
			try {
				Log.i("WifiAP","iptable, writing default values");
				BufferedWriter bufout=new BufferedWriter(new FileWriter(this.getApplicationContext().getFilesDir().toString()+"/iprules"));
				bufout.write("-tfilter -F natctrl_FORWARD\n");
				bufout.write("-tfilter -A natctrl_FORWARD -i rmnet0 -o wlan0 -m state --state RELATED,ESTABLISHED -j RETURN\n");
				bufout.write("-tfilter -A natctrl_FORWARD -i rmnet1 -o wlan0 -m state --state RELATED,ESTABLISHED -j RETURN\n");
				bufout.write("-tfilter -A natctrl_FORWARD -i wlan0 -o rmnet1 -m state --state INVALID -j DROP\n");
				bufout.write("-tfilter -A natctrl_FORWARD -i wlan0 -o rmnet0 -m state --state INVALID -j DROP\n");
				bufout.write("-tfilter -A natctrl_FORWARD -i wlan0 -o rmnet1 -j RETURN\n");
				bufout.write("-tfilter -A natctrl_FORWARD -i wlan0 -o rmnet0 -j RETURN\n");
				bufout.write("-tfilter -A natctrl_FORWARD -j DROP\n");
				bufout.write("-tnat -F natctrl_nat_POSTROUTING\n");
				bufout.write("-tnat -A natctrl_nat_POSTROUTING -s 192.168.0.0/16 -o rmnet0 -j MASQUERADE\n");
				bufout.write("-tnat -A natctrl_nat_POSTROUTING -s 192.168.0.0/16 -o rmnet1 -j MASQUERADE\n");
				bufout.write("-tnat -A natctrl_nat_POSTROUTING -o rmnet1 -j MASQUERADE\n");
				bufout.flush();
				bufout.close();
				BufferedReader  buf =new BufferedReader(new FileReader(this.getApplicationContext().getFilesDir().toString()+"/iprules"));
				String rule;
				while ((rule=buf.readLine()) != null) 	wifiAp.iprules.add(rule);
				buf.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				Log.e("WifiAP","Errori on file");
			}

		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
/*        wifiAp.iprules.add("-tfilter -F natctrl_FORWARD");
        wifiAp.iprules.add("-tfilter -A natctrl_FORWARD -i rmnet0 -o wlan0 -m state --state RELATED,ESTABLISHED -j RETURN");
        wifiAp.iprules.add("-tfilter -A natctrl_FORWARD -i rmnet1 -o wlan0 -m state --state RELATED,ESTABLISHED -j RETURN");
        wifiAp.iprules.add("-tfilter -A natctrl_FORWARD -i wlan0 -o rmnet1 -m state --state INVALID -j DROP");
        wifiAp.iprules.add("-tfilter -A natctrl_FORWARD -i wlan0 -o rmnet0 -m state --state INVALID -j DROP");
        wifiAp.iprules.add("-tfilter -A natctrl_FORWARD -i wlan0 -o rmnet1 -j RETURN");
        wifiAp.iprules.add("-tfilter -A natctrl_FORWARD -i wlan0 -o rmnet0 -j RETURN");
        wifiAp.iprules.add("-tfilter -A natctrl_FORWARD -j DROP");
        wifiAp.iprules.add("-tnat -F natctrl_nat_POSTROUTING");
        wifiAp.iprules.add("-tnat -A natctrl_nat_POSTROUTING -s 192.168.0.0/16 -o rmnet0 -j MASQUERADE");
        wifiAp.iprules.add("-tnat -A natctrl_nat_POSTROUTING -s 192.168.0.0/16 -o rmnet1 -j MASQUERADE");
        wifiAp.iprules.add("-tnat -A natctrl_nat_POSTROUTING -o rmnet1 -j MASQUERADE");
*/        
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiAp.toggleWiFiAP(wifi, this);
     }
 

}